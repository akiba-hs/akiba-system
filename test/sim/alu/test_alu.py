import os
import cocotb
from cocotb.runner import get_runner
from cocotb.triggers import Timer

from random import randint

test_dir = os.path.dirname(__file__)
rtl_dir = os.path.abspath(os.path.join(test_dir, '..', '..', '..', 'src', 'rtl'))

@cocotb.test()
async def alu_tb(dut):
    """ALU module testbench""" 
    op1 = dut.op1
    op2 = dut.op2
    ctrl = dut.ctrl
    result = dut.result
    zero = dut.zero
    
    #Addition
    ctrl.value = 0
    op1.value = randint(0, 1 << 30) # Positive result for now
    op2.value = randint(0, 1 << 30)
    await Timer(10, units='ns')
    assert int(result.value) == int(op1.value) + int(op2.value), "Addition failed!"

    #Subtraction
    ctrl.value = 1
    v2 = randint(0, 1 << 30)
    v1 = randint(v2, 1 << 30) # op1 > op2 for now
    op1.value = v1
    op2.value = v2
    await Timer(10, units='ns')
    assert int(result.value) == v1 - v2, "Subtraction failed!"

    #AND
    ctrl.value = 2
    op1.value = randint(0, 1 << 32)
    op2.value = randint(0, 1 << 32)
    await Timer(10, units='ns')
    assert int(result.value) == int(op1.value) & int(op2.value), "Bitwise AND failed!"
    
    #Zero flag
    op2.value = 0
    await Timer(10, units='ns')
    assert int(zero.value) == 1, "Zero flag failed!"

    #OR
    ctrl.value = 3
    op1.value = randint(0, 1 << 32)
    op2.value = randint(0, 1 << 32)
    await Timer(10, units='ns')
    assert int(result.value) == int(op1.value) | int(op2.value), "Bitwise OR failed!"
    

def test_alu():
    sim = os.getenv("SIM", "icarus")
    verilog_sources = [os.path.join(rtl_dir, 'alu.v')]
    runner = get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
        hdl_toplevel="alu",
        always=True,
    )

    runner.test(hdl_toplevel="alu", test_module="test_alu",)

if __name__ == "__main__":
    test_alu()

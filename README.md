# Akiba System
Processing system based on RISC-V architecture

### Useful links
<ul>
  <li>[ISA specifications](https://riscv.org/technical/specifications/)</li>
  <li>[GNU toolchain for RISC-V](https://github.com/riscv-collab/riscv-gnu-toolchain)</li>
</ul>


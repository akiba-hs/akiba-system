`timescale 1ns/100ps

module alu (
  input [31:0] op1,
  input [31:0] op2,

  input [1:0] ctrl,

  output reg [31:0] result,
  output zero
);

  `ifdef COCOTB_SIM
    initial begin
      $dumpfile("alu.vcd");
      $dumpvars(0, alu);
      #1;
    end
  `endif

  wire [31:0] n_op2, sum_op, sum;

  assign n_op2 = ~op2 + 1'b1;
  assign sum_op = ctrl[0] ? n_op2 : op2;
  assign sum = op1 + sum_op;

  always @(*) begin
    casez(ctrl[1:0])
      2'b0?: result = sum;
      2'b10: result = op1 & op2;
      2'b11: result = op1 | op2;
    endcase
  end

  assign zero = result == 32'd0;

endmodule

